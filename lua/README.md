# notafile-lua

## Requirements

* LuaJIT 2.1
* leveldb (Debian: `apt install libleveldb-dev`)
* luafilesystem (`luarocks install luafilesystem`)
* mimetypes.lua (`luarocks install mimetypes`)
* md5 (`luarocks install https://raw.githubusercontent.com/kikito/md5.lua/master/rockspecs/md5-1.1-0.rockspec`)

## Build the database

`luajit notafiledb.lua [directory]`

Recursively generates a notafile database. Unless the `[directory]` parameter is
given, it will generate the database for the current directory.

## Serve the files

`/path/to/openresty/nginx -p . -c nginx.conf -g "daemon off;"`

...or, with Docker:

`docker-compose up`

The files should be available at `http://localhost:8080/md5/{hash}`.
