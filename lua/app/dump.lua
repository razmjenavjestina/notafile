local ldb = require 'leveldb'

local md5_db = ldb.open(
  'db/md5.db',
  { create_if_missing = true, error_if_exists = false }
)

ngx.header.content_type = "application/octet-stream"

local it = md5_db:iterator({})
it:first()

local k, v = it:next()

while true do
  if k == nil then break end

  -- if the value starts with a "/", then it must be a path, and its key is its
  -- MD5 hash
  if v:sub(0, 1) == "/" then
    for i = 1, 32, 2 do
      ngx.print(
        string.char(tonumber(k:sub(i, i + 1), 16))
      )
    end
    ngx.flush()
  end
  k, v = it:next()
end

md5_db:close()
