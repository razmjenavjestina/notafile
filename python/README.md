# QuickStart

```
export PYTHONUSERBASE=$PWD/py-env
pip install --user falcon plyvel requests
```

Optionally install either gunicorn or uwsgi to run it:
```
pip install --user gunicorn
$PYTHONUSERBASE/bin/gunicorn -b localhost:7666 notafile:app
```

```
pip install --user uwsgi
$PYTHONUSERBASE/bin/uwsgi -M --enable-threads --pyargv "/directory/path" --http-socket :7666 --wsgi-file notafile.py --callable app
```

## Notes about uwsgi and pip

If you have a distribution installed `uwsgi`, you might want to add `--plugins python` or `--plugins python3` (depending
on the distribution).

On some distributions the python3 pip is `pip3` and on some it's just `pip`.
