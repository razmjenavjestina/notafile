from stem.control import Controller

with Controller.from_port(port=9051) as controller:
    controller.authenticate()
    service = controller.create_ephemeral_hidden_service({80: 7666},
                                                         key_type='NEW',
                                                         key_content='BEST',
                                                         await_publication=True)
    print("http://{}.onion".format(service.service_id))

    input('...')
